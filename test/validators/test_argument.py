# -*- coding: UTF_8 -*-

import datetime
import unittest


from pysitemapseo.validators import validate_lastmod_date, W3CDateTimeFormatException


class TestValidators(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.now = datetime.date.strftime(datetime.date.today(), '%Y-%m-%d')

    def test_validate_lastmod_date(self):
        # For each date verify that format is valid
        dates = [
            'now',
            'NOW',
            '1994-11-05T08:15:30-05:00',
            '1994-11-05T13:15:30Z',
            '2005-01-01',
            '2004-12-23T18:00:15+00:00',
            '2004-10-01T18:23:17+00:00',
            '2005',
            '2005-12',
            '2005-12-15T12:16Z',
            '2005-12-15T12:16:30Z',
            '2005-12-15T12:16:30.546Z',
            '2005-12-15T12:16+01:00',
            '2005-12-15T12:16:30+01:00',
            '2005-12-15T12:16:30.546+01:00',
            None,
        ]

        for date in dates:
            if date and date.lower() == 'now':
                self.assertEqual(self.now, validate_lastmod_date(date))
            else:
                self.assertEqual(date, validate_lastmod_date(date))

    def test_validate_lastmod_date_failed(self):
        # Verify that exception is raised
        self.assertRaises(W3CDateTimeFormatException, validate_lastmod_date, 'test bad date format')
