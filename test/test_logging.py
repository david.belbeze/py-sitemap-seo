# -*- coding: UTF_8 -*-

import logging
import mock
import unittest

from pysitemapseo.__logging import XtermStreamHandler, LoggerCapsule


class TestXtermStreamHandler(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.logger = XtermStreamHandler()

    def test_format_available(self):
        record = logging.LogRecord(
            self.logger.name,
            logging.INFO,
            '',
            0,
            'My log message',
            [],
            None
        )

        log = self.logger.format(record)

        self.assertSequenceEqual('\033[39mMy log message\033[0m', log)

    def test_format_not_available(self):
        record = logging.LogRecord(
            self.logger.name,
            -1,
            '',
            0,
            'My log message',
            [],
            None
        )

        log = self.logger.format(record)

        self.assertSequenceEqual('My log message', log)


class TestLoggerCapsule(unittest.TestCase):

    def setUp(self):
        self.logger = LoggerCapsule()

        self.mock_logger = logging.getLogger('mock logger')
        self.mock_logger.debug = mock.MagicMock()
        self.mock_logger.info = mock.MagicMock()
        self.mock_logger.warning = mock.MagicMock()
        self.mock_logger.error = mock.MagicMock()
        self.mock_logger.critical = mock.MagicMock()

        self.logger.logger = self.mock_logger

    def test_set_level(self):
        self.logger.set_level(logging.CRITICAL)

        self.assertEqual(logging.CRITICAL, self.logger.logger.level)

    def test_set_valid_logger(self):
        logger = logging.getLogger('test logger')

        self.logger.logger = logger

        self.assertEqual(logger, self.logger.logger)

    def test_set_logger_None(self):
        self.logger.logger = None

        self.assertIsNone(self.logger.logger)

    def test_raise_value_error(self):
        def to_call(instance):
            instance.logger.logger = 'bad logger'
            print instance.logger
            print instance.logger.logger
        self.assertRaises(ValueError, to_call, self)

    def test_debug(self):
        self.logger.debug('Test debug is called')

        self.mock_logger.debug.assert_called()

    def test_info(self):
        self.logger.info('Test debug is called')

        self.mock_logger.info.assert_called()

    def test_warning(self):
        self.logger.warning('Test debug is called')

        self.mock_logger.warning.assert_called()

    def test_error(self):
        self.logger.error('Test debug is called')

        self.mock_logger.error.assert_called()

    def test_critical(self):
        self.logger.critical('Test debug is called')

        self.mock_logger.critical.assert_called()
