# -*- coding: UTF_8 -*-


import urlparse
import unittest


import pysitemapseo.formatter.url as f_url


class UrlTestCase(unittest.TestCase):

    def test_get_url_full_path(self):
        urls = [
            ('http://localhost/simple/path.html', '/simple/path.html'),
            ('http://localhost/resource?query=true', '/resource?query=true'),
            ('file:///root/resource/file', '/root/resource/file')
        ]

        for url, result in urls:
            self.assertEqual(
                f_url.get_url_full_path(urlparse.urlparse(url)),
                result
            )

    def test_get_url_base(self):
        urls = [
            ('http://localhost/index.html', 'http://localhost'),
            ('http://localhost/index.php?query=true', 'http://localhost'),
            ('file://path/to/file', 'file://'),
            ('file:///path/to/file', 'file://'),
            ('/path/to/file', ''),
        ]

        for url, result in urls:
            self.assertEqual(
                f_url.get_url_base(urlparse.urlparse(url)),
                result
            )

    def test_url_encode_parsing_result(self):
        self.assertEqual(
            f_url.url_encode_parsing_result(urlparse.urlparse('http://localhost/forêt.jpeg')),
            urlparse.urlparse('http://localhost/for%C3%AAt.jpeg')
        )
        self.assertEqual(
            f_url.url_encode_parsing_result(urlparse.urlparse(u'http://localhost/forêt.jpeg')),
            urlparse.urlparse('http://localhost/for%C3%AAt.jpeg')
        )

    def test_trim_url_fragment(self):
        self.assertEqual(
            f_url.trim_url_fragment('http://localhost/index.html#contact'),
            'http://localhost/index.html'
        )
