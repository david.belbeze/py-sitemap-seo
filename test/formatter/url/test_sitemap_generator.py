# -*- coding: UTF_8 -*-

import unittest
import urlparse
import xml.etree.ElementTree as ElementTree

from pysitemapseo.formatter import SitemapGenerator


class TextSitemapGenerator(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.generator = SitemapGenerator()
        cls.namespaces = {
            'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
            'xhtml': 'http://www.w3.org/1999/xhtml',
            'image': 'http://www.google.com/schemas/sitemap-image/1.1'
        }

    def test_generate__lastmod(self):
        # This method allows to test that last mod is correctly set when is associated to url element
        urls = [
            {
                'loc': urlparse.urlparse('http://localhost/'),
                'lastmod': '2019-11-09'
            },
            {
                'loc': urlparse.urlparse('http://localhost/contact'),
            }
        ]

        root = ElementTree.fromstringlist(self.generator.generate(urls))

        # Verify that last mod is set for the first element and not for the second
        for url in root.findall('xmlns:url', self.namespaces):
            http_url = url.find('xmlns:loc', self.namespaces).text
            lastmod = url.find('xmlns:lastmod', self.namespaces)

            durl = [durl for durl in urls if durl['loc'].geturl() == http_url][0]
            self.assertEqual(durl.get('lastmod', None), lastmod.text if lastmod is not None else None)

    def test__inject_alternates(self):
        alternates = {
            'en': urlparse.urlparse('http://localhost/en/'),
            'fr': urlparse.urlparse('http://localhost/fr/')
        }

        # Generate simple site map with alternates
        sitemap = self.generator.generate(
            [
                {
                    'loc': urlparse.urlparse('http://localhost/'),
                    'alternates': alternates
                }
            ]
        )

        root = ElementTree.fromstringlist(sitemap)
        url = root.find('xmlns:url', self.namespaces)
        # For each alternate elements verify that is defined in the alternates dict
        for alternate in url.findall('xhtml:link', self.namespaces):
            self.assertEqual(alternates[alternate.get('hreflang')].geturl(), alternate.get('href'))

    def test__inject_images(self):
        # Verify that images are correctly injected in the url element
        images = [
            {
                'src': urlparse.urlparse('http://localhost/logo.png'),
                'alt': 'Logo'
            },
            {
                'src': urlparse.urlparse('http://localhost/background.png'),
            },
        ]

        root = ElementTree.fromstringlist(
            self.generator.generate(
                [
                    {
                        'loc': urlparse.urlparse('http://localhost/'),
                        'images': images
                    }
                ]
            )
        )
        url = root.find('xmlns:url', self.namespaces)

        # Verify that attributes match the image definition
        for image in url.findall('image:image', self.namespaces):
            image_url = image.find('image:loc', self.namespaces).text

            img = [img for img in images if img['src'].geturl() == image_url][0]
            # Verify that image url match the defined src url
            self.assertEqual(image_url, img['src'].geturl())

            alt_element = image.find('image:title', self.namespaces)
            self.assertEqual(img.get('alt', None), alt_element.text if alt_element is not None else None)
