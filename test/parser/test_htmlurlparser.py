# -*- coding: UTF_8 -*-

import os
import unittest
import urlparse

import pysitemapseo.parser


class TestHTMLUrlParser(unittest.TestCase):

    def setUp(self):
        self.parser = pysitemapseo.parser.HTMLUrlParser()

    def test_parse_anchor(self):
        attrs_list = [
            [('href', 'http://localhost/article/seo.html')],
            [('HREF', '/sitemap.html'), ('target', '_blank')],
            [('href', 'google-sitemap-format.html')],
            [('title', 'no href tag')]
        ]

        for attrs in attrs_list:
            self.parser.parse_anchor(attrs)

        result_urls = [
            urlparse.urlparse('http://localhost/article/seo.html'),
            urlparse.urlparse('/sitemap.html'),
            urlparse.urlparse('google-sitemap-format.html'),
        ]

        # Verify the result length
        self.assertEqual(len(self.parser.links), 3)

        # Verify individual result
        for link in self.parser.links:
            self.assertIsNone(link['hreflang'])
            self.assertIn(link['href'], result_urls)

    def test_parse_link(self):
        attrs_list = [
            [('rel', 'alternate'), ('hreflang', 'x-default'), ('Href', 'http://localhost/')],
            [('REL', 'alternate'), ('hreflang', 'en'), ('href', 'http://localhost/en')],
            [('rel', 'alternate'), ('HrefLang', 'fr'), ('href', 'http://localhost/fr')],
            [('rel', 'stylesheet'), ('href', '/assets/css/style.css')],
        ]

        for attrs in attrs_list:
            self.parser.parse_link(attrs)

        result_urls = [
            {
                'href': urlparse.urlparse('http://localhost/'),
                'hreflang': 'x-default'
            },
            {
                'href': urlparse.urlparse('http://localhost/en'),
                'hreflang': 'en'
            },
            {
                'href': urlparse.urlparse('http://localhost/fr'),
                'hreflang': 'fr'
            },
        ]

        # Verify the result length
        self.assertEqual(len(self.parser.links), 3)

        # Verify individual result
        for link in self.parser.links:
            self.assertDictEqual(link, self.__find_result_link(result_urls, link['hreflang']))

    def test_parse_img(self):
        attrs_list = [
            [('src', '/assets/logo.png')],
            [('SRC', '/assets/logo.png')],
            [('src', '/assets/background.jpeg'), ('alt', 'Landscape picture')],
        ]

        for attrs in attrs_list:
            self.parser.parse_img(attrs)

        result_images = [
            {
                'src': urlparse.urlparse('/assets/logo.png'),
                'alt': None
            },
            {
                'src': urlparse.urlparse('/assets/background.jpeg'),
                'alt': 'Landscape picture'
            },
        ]

        # Verify the result length
        self.assertEqual(len(self.parser.images), 2)

        # Verify individual result
        for image in self.parser.images:
            self.assertDictEqual(image, self.__find_result_image(result_images, image['src']))

    def test_feed(self):
        html_path = os.path.join(os.path.dirname(__file__), '..', 'resources', 'index.html')
        with open(html_path, 'rb') as f:
            self.parser.feed(f.read())

        # Verify the correct extraction
        # links
        self.assertListEqual(
            self.parser.links,
            [
                {'hreflang': 'x-default', 'href': urlparse.urlparse('http://localhost/')},
                {'hreflang': 'en', 'href': urlparse.urlparse('http://localhost/en')},
                {'hreflang': 'fr', 'href': urlparse.urlparse('http://localhost/fr')},
                {'hreflang': None, 'href': urlparse.urlparse('/en')},
                {'hreflang': None, 'href': urlparse.urlparse('http://localhost/fr')}
            ]
        )
        # images
        self.assertListEqual(
            self.parser.images,
            [
                {'src': urlparse.urlparse('./assets/cover.jpeg'), 'alt': 'Image representing SEO with python'},
                {'src': urlparse.urlparse('/assets/log-python.png'), 'alt': 'Python logo'}
            ]
        )

    @staticmethod
    def __find_result_link(results, locale):
        for item in results:
            if item['hreflang'] == locale:
                return item
        return None

    @staticmethod
    def __find_result_image(results, src):
        for item in results:
            if item['src'] == src:
                return item
        return None
