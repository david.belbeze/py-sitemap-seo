#!/bin/sh

# Init test environment
cd $(dirname $(readlink -f $0))/../
. venv/bin/activate

# Run tests
coverage erase
coverage run --include=pysitemapseo/* -m unittest discover
coverage html
coverage xml

# close venv
deactivate