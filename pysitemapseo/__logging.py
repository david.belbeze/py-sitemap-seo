# -*- coding: UTF_8 -*-

import logging
import sys


class XtermStreamHandler(logging.StreamHandler):

    __DECOR = {
        logging.DEBUG: '\033[37m{msg}\033[0m',
        logging.INFO: '\033[39m{msg}\033[0m',
        logging.WARNING: '\033[33m{msg}\033[0m',
        logging.ERROR: '\033[31m{msg}\033[0m',
        logging.CRITICAL: '\033[1m\033[31m{msg}\033[0m',
    }

    def format(self, record):
        sequence = logging.StreamHandler.format(self, record)

        pattern = self.__DECOR.get(record.levelno)

        if pattern is not None:
            return pattern.format(msg=sequence)
        else:
            return sequence


# Init the default logger
default_logger = logging.getLogger('SiteMapperLogger')

out_hdlr = XtermStreamHandler(sys.stderr)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s - %(levelname)s: %(message)s'))
out_hdlr.setLevel(logging.DEBUG)

default_logger.addHandler(out_hdlr)
default_logger.setLevel(logging.INFO)


class LoggerCapsule(object):
    """
    This class allows to encapsulate a logger and prevent errors if the logger is NoneType.
    """

    def __init__(self, logger=None):
        """
        :param logger: the logger to encapsulate
        :type logger: logging.Logger
        """
        self.__set_logger(logger)

    def __set_logger(self, logger):
        """
        This method allows to set the logger when class match logging.Logger class

        :param logger: the logger to encapsulate
        :raise ValueError: when the logger is not logger.Logger class
        """
        if isinstance(logger, logging.Logger) or logger is None:
            self._logger = logger
        else:
            raise ValueError('{} is not an instance of {}'.format(logger.__class__, logging.Logger.__class__))

    def set_level(self, level):
        """
        Set the log level of the logger.

        :param level: the log level to set on embedded logger
        """
        if self._logger is not None:
            self._logger.setLevel(level)

    def debug(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'DEBUG'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.debug("Houston, we have a %s", "thorny problem", exc_info=1)
        """
        self.__log('debug', msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'INFO'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.info("Houston, we have a %s", "interesting problem", exc_info=1)
        """
        self.__log('info', msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'WARNING'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.warning("Houston, we have a %s", "bit of a problem", exc_info=1)
        """
        self.__log('warning', msg, *args, **kwargs)

    def error(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'ERROR'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.error("Houston, we have a %s", "major problem", exc_info=1)
        """
        self.__log('error', msg, *args, **kwargs)

    def critical(self, msg, *args, **kwargs):
        """
        Log 'msg % args' with severity 'CRITICAL'.

        To pass exception information, use the keyword argument exc_info with
        a true value, e.g.

        logger.critical("Houston, we have a %s", "major disaster", exc_info=1)
        """
        self.__log('critical', msg, *args, **kwargs)

    def __log(self, level, msg, *args, **kwargs):
        """
        This method allows to log the message as the given level.

        :param level: the name of the method to call
        :param msg: the message to log
        :type level: str
        :type msg: str
        """
        if self._logger is not None:
            func = getattr(self._logger, level, None)
            if func:
                func(msg, *args, **kwargs)

    @property
    def logger(self):
        return self._logger

    @logger.setter
    def logger(self, value):
        self.__set_logger(value)
