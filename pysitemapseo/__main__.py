#!/usr/bin/env python
# -*- coding: UTF_8 -*-

import argparse
import sys

import pysitemapseo
from pysitemapseo.validators import validate_lastmod_date, W3CDateTimeFormatException
from pysitemapseo.formatter import SitemapGenerator


def main():
    parser = argparse.ArgumentParser("pysitemapseo")

    parser.add_argument(
        "--input",
        "-i",
        required=True,
        help="The input parameter is the url as entry point of the website to scan. In general is the root website url."
    )
    parser.add_argument(
        "--output",
        "-o",
        help="This option allows to define the path of the sitemap.xml file generated. The path can be a directory "
             "path or a file path. When is a file path the extension \".xml\" must be used otherwise it will be "
             "interpreted as a directory",
        default=None
    )
    parser.add_argument(
        '--insecure',
        '-k',
        help='Allow connections to SSL sites without certs',
        action='store_true',
        default=False
    )
    parser.add_argument(
        '--lastmod',
        '-m',
        help='Provide the <lastmod> value to set for each <url> tag, possible values "now" or W3C date format',
        default=None
    )
    parser.add_argument(
        '--verbose',
        '-v',
        help='Use this flag to access to debug log level',
        action='store_true'
    )

    namespace = parser.parse_args()

    # Verify the lastmod argument format
    try:
        lastmod = validate_lastmod_date(namespace.lastmod)
    except W3CDateTimeFormatException as e:
        print('{}\n'.format(e))
        parser.print_help()
        sys.exit(1)
    else:
        sitemap = pysitemapseo.WebsiteScanner(
            namespace.input,
            lastmod=lastmod,
            strict_ssl=not namespace.insecure,
            debug=namespace.verbose
        )
        sitemap.scan()

        sitemap_sequence = SitemapGenerator().generate(sitemap.urlset)
        if namespace.output is None:
            # write on stdout
            print(sitemap_sequence)
        else:
            # try yo write to the specified file
            try:
                with open(namespace.output, 'wb') as f:
                    f.write(sitemap_sequence)
            except IOError as e:
                print(e, file=sys.stderr)
                sys.exit(1)


if __name__ == '__main__':
    main()
