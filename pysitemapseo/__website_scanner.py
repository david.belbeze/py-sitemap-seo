# -*- coding: UTF_8 -*-

import http.cookiejar
import http.client
import html.parser
import logging
import re
import urllib.request, urllib.error, urllib.parse
import urllib.parse
import ssl
import traceback
import sys

from pysitemapseo.parser import HTMLUrlParser, RegexUrlParser
from pysitemapseo.__logging import default_logger, LoggerCapsule
from pysitemapseo.formatter.url import url_encode_parsing_result, get_url_full_path


class WebsiteScanner:
    """
    The website scanner class can crowls on website to extract url, alternates and images
    """

    def __init__(self, base, lastmod=None, logger=default_logger, strict_ssl=True, debug=False):
        # Correct the url to have '/' if the path is empty
        self.base = urllib.parse.urlparse(base)
        if self.base.path == '':
            self.base = urllib.parse.ParseResult(
                self.base.scheme,
                self.base.netloc,
                '/',
                self.base.params,
                self.base.query,
                self.base.fragment
            )

        # Prepare urls to scan and resource to collect
        self._cursor = 0
        self._urls = [
            self.base
        ]

        self._alternates = {}
        self._images = {}
        self._not_html = []

        # Prepare the handler to scan all the website
        handlers = []

        self._cookie_jar = http.cookiejar.CookieJar()
        handlers.append(urllib.request.HTTPCookieProcessor(self._cookie_jar))

        handlers.append(urllib.request.HTTPRedirectHandler())

        if not strict_ssl:
            # Prevent error for self signed CA
            ssl_context = ssl.create_default_context()
            ssl_context.check_hostname = False
            ssl_context.verify_mode = ssl.CERT_NONE

            handlers.append(urllib.request.HTTPSHandler(context=ssl_context))

        self._opener = urllib.request.build_opener(*handlers)

        self.__logger = LoggerCapsule(logger)
        if debug:
            self.__logger.set_level(logging.DEBUG)

        self.lastmod = lastmod
        self._urlset = None

    def scan(self):
        """
        The method allows to scan the website from the entry url.
        Each new url with the same dns are collected to be scanned as well.
        """
        loop = True
        while loop:
            url = self._next_url()

            if url is None:
                loop = False
            else:
                self._scan_page(url)

    def _next_url(self):
        """
        This method allows to get the next url to analyse or None if there is no more urls to access

        :return: the url object to analyse or None is there is no urls to look
        """
        url = None
        if self._cursor < len(self._urls):
            url = self._urls[self._cursor]
            self._cursor += 1

            self.__logger.debug(
                'Forecast progression:\t{}/{}\t\t{:.02%}'.format(
                    self._cursor,
                    len(self._urls),
                    float(self._cursor) / len(self._urls)
                )
            )
        return url

    def _scan_page(self, url):
        """
        This method allows to collect all links on the web page and metadata associated to this link.

        Url on anchors are scanned
        Url on <link rel="alternate" ...> tag are collected
        Url of image src as well

        :param url: the url of the web page to scan
        :type url: urlparse.ParseResult
        """
        try:
            uri = url.geturl()

            self.__logger.info('Start analysis of {}'.format(uri.encode('utf-8')))

            # Get the web page content
            request = urllib.request.Request(uri)

            with self._opener.open(request) as fd:
                content_type = fd.headers['Content-Type']
                if content_type.lower().startswith('text/html'):
                    html_content = fd.read()

                    # Prevent encoding errors by decoding the string sequence
                    match = re.search(r'charset=(?P<encoding>[^ ]+)', content_type)
                    if match:
                        encoding = match.groupdict()['encoding']
                        html_content = html_content.decode(encoding)

                    # Parse the html content to extract anchors, link and image src urls
                    try:
                        parser = HTMLUrlParser()
                        parser.feed(html_content)

                        for link in parser.links:
                            curl = link['href']
                            if curl.netloc == '':
                                link['href'] = urllib.parse.urlparse(
                                    urllib.parse.urljoin(fd.geturl(), get_url_full_path(curl))
                                )

                        # collect links and images
                        links = parser.links
                        images = parser.images
                    except html.parser.HTMLParseError as e:
                        # Log warning to indicate this web page have an invalid html format
                        self.__logger.warning(e)

                        # The page cannot be parsed with the HTMLParser use the method with regular expressions
                        parser = RegexUrlParser()
                        parser.from_string(html_content)

                        links = parser.links
                        images = parser.images
                    except Exception as e:
                        self.__logger.critical(e)

                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        traceback.print_exception(exc_type, exc_value, exc_traceback, file=sys.stderr)

                        links = []
                        images = []
                    finally:
                        self.__register_urls(
                            url,
                            [link for link in links if link['href'].netloc == self.base.netloc],
                            images
                        )
                else:
                    # Tag the current url as "not html"
                    self._not_html.append(url)
                    self.__logger.info('This link isn\'t an html page')
        except urllib.error.URLError as e:
            self.__logger.error(e)
        except http.client.IncompleteRead as e:
            self.__logger.error(e)

    def __register_urls(self, url, links, images):
        """
        This method collect new web page link to scan them latter, register alternates and images src found on the web
        page corresponding to the current url web page.

        :param url: the current url
        :param links: url found in the anchor and link tags
        :param images: the list of img tag description, each item must contain src and alt attributes
        :return:
        """
        alternates = {}

        statistic = {
            'link': 0,
            'images': 0,
            'alternates': 0
        }

        if url not in self._images:
            # if the url is relative add the full scheme and domain
            self.__correct_image_url(url, images)
            self._images[url] = images

        for link in links:
            url, locale = link['href'], link['hreflang']
            # Register the link if is needed
            if url not in self._urls:
                self._urls.append(url_encode_parsing_result(url))
                statistic['link'] += 1
                self.__logger.debug('New url to scan: {}'.format(url.geturl().encode('utf-8')))

            # Populate the alternates object
            if locale is not None:
                alternates[locale] = url

                if locale == 'x-default':
                    if url in self._alternates:
                        alternates.update(self._alternates[url])
                    # Update/create the dict reference
                    self._alternates[url] = alternates

        statistic['images'] += len(images)
        statistic['alternates'] = len(alternates)

        formatted_stat = self.__format_statistic(statistic)
        self.__logger.info(
            "Page scan completed"
            + (('\n' + formatted_stat) if formatted_stat else '')
        )

    @property
    def urlset(self):
        if self._urlset is None:
            self._urlset = []
            self.__create_urls()
        return self._urlset

    def __create_urls(self):
        """
        This method allows to pass on each urls collected and create the url tag for urlset of the sitemap.
        """
        for loc in self._urls:
            if loc not in self._not_html:
                url_tag = {
                    'loc': loc,
                }

                # Set the modification
                if self.lastmod is not None:
                    url_tag['lastmod'] = self.lastmod

                # Append the alternates links
                if loc in self._alternates:
                    url_tag['alternates'] = self._alternates[loc]

                # Append the images links
                if loc in self._images:
                    url_tag['images'] = self._images[loc]

                self._urlset.append(url_tag)

    @staticmethod
    def __correct_image_url(url, images):
        """
        Modify the image src url by setting the scheme and domain when is not set

        :param url: the web page url
        :param images: the list of images extracted from the web page corresponding to url
        """
        for image in images:
            if image['src'].netloc == '':
                image['src'] = urllib.parse.urlparse(
                    urllib.parse.urljoin('{}://{}/'.format(url.scheme, url.netloc), image['src'].geturl(), False)
                )

    @staticmethod
    def __format_statistic(statistic):
        """
        Format the statistics for logger.

        :param statistic: the dict containing stats
        :type statistic: dict
        :return: the statistics to formatted
        :rtype: str
        """
        lines = []

        if statistic['link'] > 0:
            lines.append('{} link(s) found for analysis'.format(statistic['link']))
        if statistic['images'] > 0:
            lines.append('{} image(s) found'.format(statistic['images']))
        if statistic['alternates'] > 0:
            lines.append('{} alternate(s) found'.format(statistic['alternates']))

        return '\n'.join([('    - ' + line) for line in lines])
