# -*- coding: UTF_8 -*-

# This module contains function and classes that allows to validate arguments for the pysitemapseo module

import datetime
import re

from .__exceptions import W3CDateTimeFormatException


W3C_DATE_FORMAT = re.compile(r"^(?P<year>[0-9]{4})(-(?P<month>[0-9]{2})(-(?P<day>[0-9]{2})(T(?P<hour>[0-9]{2}):"
                             r"(?P<minute>[0-9]{2})(:(?P<second>[0-9]{2})(\.(?P<millisecond>[0-9]{1,3}))?)?"
                             r"(?P<timezone>Z|[+-][0-9]{2}:[0-9]{2})?)?)?)?$")


def validate_lastmod_date(date):
    """
    This method allows to validate the lastmod date provided in parameter.

    :param date: the string representing the date as <a href="https://www.w3.org/TR/NOTE-datetime">W3C date format</a>
                 or None
    :type date: str|None
    :return: the date or datetime associated to the given date param formatted in W3C date or datetime format
    """
    corrected_date = None

    if date is not None:
        if date.lower().strip() == 'now':
            corrected_date = datetime.date.strftime(datetime.date.today(), '%Y-%m-%d')
        else:
            match = W3C_DATE_FORMAT.match(date)
            if match:
                # The date match the pattern
                corrected_date = date
            else:
                raise W3CDateTimeFormatException(
                    'The date "{}" not match the W3C date time format pattern'.format(date)
                )

    return corrected_date

