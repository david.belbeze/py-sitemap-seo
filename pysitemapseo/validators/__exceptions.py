# -*- coding: UTF_8 -*-


class W3CDateTimeFormatException(BaseException):
    """Raise this exception when the W3C date time format is invalid"""
