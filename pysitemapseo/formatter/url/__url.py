# -*- coding: UTF_8 -*-

import urllib.parse
import urllib.request, urllib.parse, urllib.error
import re


def get_url_full_path(url):
    """
    Analyse the url and return the full path.

    :param url: the url to analyse
    :type url: urlparse.ParseResult
    """
    if isinstance(url.path, str):
        path = url.path.encode('utf-8')
    else:
        path = url.path
    path = urllib.parse.quote(path)
    if url.query:
        full_path = path + '?' + url.query
    else:
        full_path = path
    return full_path


def get_url_base(url):
    """
    Analyse the url and return the base.

    :param url: the url to analyse
    :type url: urlparse.ParseResult
    :return:
    """
    if url.netloc:
        postfix = url.netloc
    else:
        postfix = ''

    if url.scheme:
        if url.scheme == 'file':
            return url.scheme + '://'
        else:
            return url.scheme + '://' + postfix
    else:
        return postfix


def url_encode_parsing_result(url):
    """

    :param url: the url to analyse
    :type url: urlparse.ParseResult
    :return: the corrected url parse result
    """
    return urllib.parse.urlparse(urllib.parse.urljoin(get_url_base(url), get_url_full_path(url), False))


def trim_url_fragment(url):
    """

    :param url:
    :return:
    """
    return re.sub(r'([^#]*)#[^/]*$', r'\1', url)
