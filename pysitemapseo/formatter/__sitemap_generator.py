# -*- coding: UTF_8 -*-

import xml.etree.ElementTree as ElementTree
import xml.dom.minidom as minidom


class SitemapGenerator:
    """
    The Sitemap Generator class allows to create a xml sequence with configuration parameter
    """

    def __init__(self, indent=' ' * 4, ln='\n', encoding='utf-8'):
        self._indent = indent
        self._ln = ln
        self._encoding = encoding

    def generate(self, data_set):
        """
        This method allows to generate an ElementTree containing information from the dataset parameter.

        :param data_set: the list of entities to inject in the ElementTree object
        :type data_set: list[dict]
        :return: the ElementTree root item
        :rtype: str
        """
        root = ElementTree.Element('urlset')
        root.set('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9')

        config = {
            'alternates': False,
            'images': False,
        }

        # For each url element, create a new url tag in the urlset tag
        for url in data_set:
            url_tree = ElementTree.SubElement(root, 'url')

            # Set the loc item
            loc_element = ElementTree.SubElement(url_tree, 'loc')
            loc_element.text = url['loc'].geturl()

            # Set the lastmod if necessary
            if 'lastmod' in url:
                lastmod_element = ElementTree.SubElement(url_tree, 'lastmod')
                lastmod_element.text = url['lastmod']

            # Inject locale alternates
            if 'alternates' in url:
                config['alternates'] = True
                self.__inject_alternates(url_tree, url['alternates'])

            # Inject images references
            if 'images' in url:
                config['images'] = True
                self.__inject_images(url_tree, url['images'])

        for key in config:
            # For each config enabled, add the corresponding namespace
            if config[key]:
                # the namespace must be set
                func = {
                    'alternates': lambda r: r.set('xmlns:xhtml', 'http://www.w3.org/1999/xhtml'),
                    'images': lambda r: r.set('xmlns:image', 'http://www.google.com/schemas/sitemap-image/1.1'),
                }.get(key)

                if func is not None:
                    func(root)

        # Return formatted xml
        return minidom.parseString(ElementTree.tostring(root, self._encoding))\
            .toprettyxml(indent=self._indent, newl=self._ln, encoding=self._encoding)

    @staticmethod
    def __inject_alternates(url_element, alternates):
        """
        This method inject the list of alternate items in the url parent element as the google alternate specification.

        :param url_element: the url element parent
        :param alternates: the list of alternates
        :type url_element: ElementTree.Element
        :type alternates: dict[string, urlparse.ParseResult]
        """
        for alternate in alternates:
            ElementTree.SubElement(
                url_element,
                'xhtml:link',
                {
                    'rel': 'alternate',
                    'hreflang': alternate,
                    'href': alternates[alternate].geturl()
                }
            )

    @staticmethod
    def __inject_images(url_element, images):
        """
        For each images, the corresponding 'image:image' tag is added to the url element.

        :param url_element: the parent url element
        :param images: the list of images collected
        :type url_element: ElementTree.Element
        :type images: list[dict]
        """
        for image in images:
            image_element = ElementTree.SubElement(url_element, 'image:image')

            image_loc = ElementTree.SubElement(image_element, 'image:loc')
            image_loc.text = image['src'].geturl()

            if image.get('alt') is not None:
                image_title = ElementTree.SubElement(image_element, 'image:title')
                image_title.text = image['alt']
