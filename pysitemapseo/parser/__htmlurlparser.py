# -*- coding: UTF_8 -*-

import urllib.parse
from html.parser import HTMLParser

from pysitemapseo.formatter.url import url_encode_parsing_result, trim_url_fragment


class HTMLUrlParser(HTMLParser):
    """
    This class allows to parse an html sequence and extract anchors href, links href and image src.
    """

    def __init__(self):
        HTMLParser.__init__(self)

        self._links = []
        self._images = []

        self.parsers = {
            'a': self.parse_anchor,
            'link': self.parse_link,
            'img': self.parse_img
        }

    @property
    def links(self):
        return self._links

    @property
    def images(self):
        return self._images

    def handle_starttag(self, tag, attrs):
        parser = self.parsers.get(tag.lower(), None)

        if parser is not None:
            parser(attrs)

    def parse_anchor(self, attrs):
        """
        Analyses all attributes of the "a" tag.

        :param attrs: the list of attributes of the tag
        :type attrs: list[tuple[str, str]]
        """
        for attr, value in attrs:
            if attr.lower() == 'href':
                # Add only real url
                if not value.lower().startswith('mailto:'):
                    self._links.append({
                        'href': urllib.parse.urlparse(trim_url_fragment(value)),
                        'hreflang': None
                    })
                break

    def parse_link(self, attrs):
        """
        Analyses all attributes of the "link" tag.

        :param attrs: the list of attributes of the tag
        :type attrs: list[tuple[str, str]]
        """
        description = {
            'hreflang': None
        }

        for attr, value in attrs:
            attr = attr.lower()
            if attr == 'rel' and value.lower() == 'alternate':
                self._links.append(description)
            elif attr == 'href':
                description['href'] = urllib.parse.urlparse(trim_url_fragment(value))
            elif attr == 'hreflang':
                description['hreflang'] = value

    def parse_img(self, attrs):
        """
        Analyses all attributes of the "img" tag.

        :param attrs: the list of attributes of the tag
        :type attrs: list[tuple[str, str]]
        """
        image = {
            'src': None,
            'alt': None
        }

        for attr, value in attrs:
            attr = attr.lower()
            if attr == 'src':
                image['src'] = url_encode_parsing_result(urllib.parse.urlparse(value))
            if attr == 'alt':
                image['alt'] = value

        # Append the image to the list if the url is not already set
        for img in self._images:
            if image['src'] == img['src']:
                break
        else:
            self._images.append(image)
